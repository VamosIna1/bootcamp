package com.sudomakmur.bootcamp

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.sudomakmur.bootcamp.Constant.DATA_DARI_LOGIN
import com.sudomakmur.bootcamp.model.Register
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*




class RegisterActivity : AppCompatActivity(),AdapterView.OnItemSelectedListener{

    var myCalendar : Calendar ? = null

    val dataDariLogin : Int? by lazy {

        intent.getIntExtra(DATA_DARI_LOGIN,0)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        onRadioButtonCliked()
        onSpinnerAttach()
        onBirthdateSeleted()

        editTextUsername.setText(dataDariLogin.toString())

        checkTermCondition.setOnClickListener {
            val registerData = Register(
                email = editTextEmail.text.toString(),
                username = editTextUsername.text.toString(),
                password = editTextPassword.text.toString()
            )
            val i = Intent(this,TermAndCondition::class.java)
            i.putExtra("dataRegister",registerData )
            startActivity(i)
        }


        btnRegiterAccount.setOnClickListener {
            if (checkTermCondition.isChecked){
                val i = Intent(this,TermAndCondition::class.java)
                i.putExtra("username",editTextUsername.text.toString())
                i.putExtra("email", editTextConfirmEmail.text.toString())
                i.putExtra("password",editTextPassword.text.toString())
                startActivity(i)
            }else{
                Toast.makeText(this@RegisterActivity,getString(R.string.str_ceklist_term_condition),Toast.LENGTH_SHORT).show()
            }
        }


        myCalendar = Calendar.getInstance()
    }

    private fun onBirthdateSeleted() {
        val date = OnDateSetListener { view, year, monthYear, datOfMonth ->
            {
                myCalendar?.let {
                    myCalendar!![Calendar.YEAR] = year
                    myCalendar!![Calendar.MONTH] = monthYear
                    myCalendar!![Calendar.DAY_OF_MONTH] = datOfMonth
                }

            }
            updateLabel()
        }


        textBirthDate.setOnClickListener {
            myCalendar?.let {
               DatePickerDialog(this,date,myCalendar!!.get(Calendar.YEAR),
                    myCalendar!!.get(Calendar.MONTH),myCalendar!!.get(Calendar.DAY_OF_MONTH)).show()


            }

        }


    }


    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()
    }


    override fun onPause() {
        super.onPause()
    }


    override fun onRestart() {
        super.onRestart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun updateLabel() {
        val format = "MM/dd/yy"
        val sdf = SimpleDateFormat(format,Locale.US)

        textBirthDate.setText(sdf.format(myCalendar?.time))
    }

    private fun onSpinnerAttach() {
        val works = resources.getStringArray(R.array.works)

        val dataAdapter : ArrayAdapter<String> = ArrayAdapter(this,android.R.layout.simple_spinner_item,works)

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        spnrBtnJob.adapter = dataAdapter
    }

    private fun onRadioButtonCliked() {
        rdGroupGender.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                when(checkedId){
                    R.id.rdBtnMan ->{
                        Toast.makeText(this@RegisterActivity,rdBtnMan.text,Toast.LENGTH_SHORT).show()
                    }
                    R.id.rdBtnWoman -> {
                        Toast.makeText(this@RegisterActivity,rdBtnWoman.text,Toast.LENGTH_SHORT).show()
                    }
                }
            }

        })
    }


    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

    }

    companion object{

    }

}