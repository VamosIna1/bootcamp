package com.sudomakmur.bootcamp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sudomakmur.bootcamp.model.Register
import kotlinx.android.synthetic.main.activity_term_and_condition.*

class TermAndCondition : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_term_and_condition)

//        webViewTermAndCondition.loadUrl("files:///android_asset/TermAndCondition.html")

//        webViewTermAndCondition.loadUrl("file:///android_asset/TermAndCondition.html")

        val registerData = intent.getSerializableExtra("dataRegister" ) as Register
         registerData?.let {
             textValueUserName.text = it.username
             textValueEmail.text=it.email
             textValueUserPassword.text=it.password
         }

    }
}