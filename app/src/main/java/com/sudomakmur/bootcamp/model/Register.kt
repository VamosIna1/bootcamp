package com.sudomakmur.bootcamp.model

import java.io.Serializable

data class Register (
    val username:String,
    val email:String,
    val password:String
        ): Serializable