package com.sudomakmur.bootcamp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.sudomakmur.bootcamp.Constant.DATA_DARI_LOGIN
import kotlinx.android.synthetic.main.activity_login.*

class AnySizeActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnRegiterAccount.setOnClickListener {
           val intent = Intent(this,RegisterActivity::class.java)
            intent.putExtra(DATA_DARI_LOGIN,10)
            startActivity(intent)
        }
    }
}